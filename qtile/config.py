########## Imports

import os
import subprocess
from libqtile import bar, extension, layout, widget, hook, qtile, pangocffi
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from os.path import expanduser
from subprocess import Popen
from libqtile.log_utils import logger
from libqtile.widget import base
from qtile_extras import widget
import colors

########## Border Decorations ########################################

from qtile_extras.widget.decorations import BorderDecoration

########## Weather Icons #############################################

from owm import owm_symbols

######################################################################

mod = "mod4"
alt = "mod1"
terminal = "kitty" #guess_terminal()
myBrowser = "firefox"

######################################################################

########## Autostarting Programs #####################################

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.Popen([home])

######################################################################

########## Clock Mouseover ###########################################

class MouseOverClock(widget.Clock):
    defaults = [
        ("long_format", "%d %B %A %Y | %H:%M")
    ]

    def __init__(self, **config):
        widget.Clock.__init__(self, **config)
        self.add_defaults(MouseOverClock.defaults)
        self.short_format = self.format

    def mouse_enter(self, *args, **kwargs):
        self.format = self.long_format
        self.bar.draw()

    def mouse_leave(self, *args, **kwargs):
        self.format = self.short_format
        self.bar.draw()

######################################################################

########## Volume Control ############################################

class MyVolume(widget.Volume):
    def _configure(self, qtile, bar):
        widget.Volume._configure(self, qtile, bar)
        self.volume = self.get_volume()
        if self.volume <= 0:
            self.text = '󰆪'
        elif self.volume <= 15:
            self.text = ''
        elif self.volume < 50:
            self.text = ''
        else:
            self.text = ''
        # drawing here crashes Wayland

    def _update_drawer(self, wob=False):
        if self.volume <= 0:
            self.text = '󰆪'
        elif self.volume <= 15:
            self.text = ''
        elif self.volume < 50:
            self.text = ''
        else:
            self.text = ''
        self.draw()

        if wob:
            with open(self.wob, 'a') as f:
                f.write(str(self.volume) + "\n")

######################################################################


keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "Left", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "Right", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "Down", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod, "control"], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen on the focused window"),

########## My Custom Keybindings ##########

    Key([mod], "r", lazy.spawn("rofi -show drun -show-icons"), desc="Launch Rofi"),
    Key([mod], "f", lazy.spawn("brave"), desc="Launch Browser"),
    Key([mod], "t", lazy.spawn("thunar"), desc="Launch Thunar"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -D pulse sset Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -D pulse sset Master 5%+")),
    Key([], "XF86AudioMute", lazy.spawn("amixer sset Master 1+ toggle"), desc="Mute/Unmute Volume"),
#   Key([], "XF86AudioMute", lazy.spawn("pulseaudio-ctl mute")),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause"), desc="Play/Pause player"),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next"), desc="Skip to next"),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous"), desc="Skip to previous"),
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl s +5%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl s 5%-")),
    Key([alt], "Shift_L", lazy.widget["keyboardlayout"].next_keyboard(), desc="Next keyboard layout."),
    Key([mod, "shift"], "q", lazy.spawn("qbittorrent"), desc="Launch Qbittorrent"),
    Key([mod, "shift"], "c", lazy.spawn(terminal + ' -e cava'), desc="Launch Qbittorrent"),

######################################################################
################## Rhythmbox Controls ################################

    Key([mod, "shift"], "r", lazy.spawn("rhythmbox"), desc="Launch Rhythmbox"),
    Key([], "XF86AudioPlay", lazy.spawn("rhythmbox-client --play-pause"), desc="Play/Pause player"),
    Key([], "XF86AudioNext", lazy.spawn("rhythmbox-client --next"), desc="Skip to next"),
    Key([], "XF86AudioPrev", lazy.spawn("rhythmbox-client --previous"), desc="Skip to previous"),
    Key([], "XF86AudioStop", lazy.spawn("rhythmbox-client --quit"), desc="Skip to previous"),

######################################################################

]

groups = []
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9",]

#group_labels = ["DEV", "WWW", "SYS", "DOC", "VBOX", "CHAT", "MUS", "VID", "GFX",]
#group_labels = ["1", "2", "3", "4", "5", "6", "7", "8", "9",]
group_labels = ["", "", "", "", "", "", "", "", "",]


group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall"]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))
 
for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Move focused window to group {}".format(i.name),

            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

######################################################################
################## LAYOUTS ###########################################

layouts = [

######################################################################
    
################## Columns Layout ####################################

    layout.Columns(
                   border_focus='#bbff00',
                   border_focus_stack='#00fff5',
                   border_normal='#ff0093', 
                   border_normal_stack='#00fff5',
                   border_on_single='#bbff00',
                   border_width=2, 
                   margin= 5,
                   margin_on_single=7,
		   split= True,),

######################################################################

################## Max Layout ########################################

        layout.Max(
                   border_focus='#bbff00',
                   border_focus_stack='#00fff5',
                   border_normal='#ff0093', 
                   border_normal_stack='#00fff5',
                   border_on_single='#bbff00',
                   border_width=2, 
                   margin=6,
                   margin_on_single=7,),

######################################################################

################## Matrix Layout #####################################

    layout.Matrix(
                  border_focus='#bbff00',
                  border_focus_stack='#00fff5',
                  border_normal='#ff0093', 
                  border_normal_stack='#00fff5',
                  border_on_single='#bbff00',
                  border_width=2, 
                  margin=6,
                  margin_on_single=7,),

######################################################################

################## MonadTall Layout ##################################

    layout.MonadTall(
                  border_focus='#bbff00',
                  border_focus_stack='#00fff5',
                  border_normal='#ff0093', 
                  border_normal_stack='#00fff5',
                  border_on_single='#bbff00',
                  border_width=2,
		  change_ratio= 0.05, 
                  margin=8,
                  margin_on_single=8,),

######################################################################

################## MonadWide Layout ##################################

    layout.MonadWide(
                  border_focus='#bbff00',
                  border_focus_stack='#00fff5',
                  border_normal='#ff0093', 
                  border_normal_stack='#00fff5',
                  border_on_single='#bbff00',
                  border_width=2, 
                  margin=6,
                  margin_on_single=7,),

######################################################################

################## RatioTile Layout ##################################

    layout.RatioTile(
                  border_focus='#bbff00',
                  border_focus_stack='#00fff5',
                  border_normal='#ff0093', 
                  border_normal_stack='#00fff5',
                  border_on_single='#bbff00',
                  border_width=2, 
                  margin=6,
                  margin_on_single=7,),

    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),

######################################################################

]

widget_defaults = dict(
    		  font= "RoundorNonCommercial",
    		  fontsize=14,
    		  padding= 2,)
extension_defaults = widget_defaults.copy()


screens = [
    Screen(
        top=bar.Bar(
            [
             
########## Separator #################################################		

		widget.Sep(
			       foreground= '#24283b',
			       background= '#24283b',),

######################################################################

########## Distro Logo ###############################################

		widget.TextBox(
                    	       text=' ',
                    	       fontsize = 18,
                    	       padding= 0,
                    	       font='FontAwesome',
			       foreground= '#0041ff',),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#ff0000',
				background= '#24283b',
				linewidth= 2,
				size_percent= 50,
				padding= 2,),

######################################################################

########## Current Layout Icon #######################################

#		widget.CurrentLayoutIcon(
#				font= 'sans',
#				fontsize= 3,),

######################################################################

########## Current Layout ############################################
	       
		widget.CurrentLayout(
				font= '01 DigitGraphics',
				fontsize= 15,
				foreground= '#ff9809',),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#ff0000',
				background= '#24283b',
				linewidth= 2,
				size_percent= 50,
				padding= 2,),

######################################################################

########## Workspaces ################################################

                widget.GroupBox(
				active='#00fff5',
				block_highlight_text_color='#bbff00',
				borderwidth= 0,
				font= 'FontAwesome',
				foreground= '#24283b',
				highlight_color= ['#24283b', '#24283b'],
				highlight_method= 'line',
				border_color= '#fcff00',
				inactive= '#ff0093',
				padding_x= 2,
				padding_y= 3,
				fontsize= 14,
				urgent_border= '#ff000',),

######################################################################

                widget.Prompt(),

######################################################################

########## Window Title ##############################################

#                widget.WindowName(
#				format= '{state}{name}',
#				foreground= '#fcff00',
#				max_chars = 40,),

######################################################################

########## Window Task List ##############################################

		widget.TaskList(
				border= '#ff9809',
				font= 'JetBrains Mono Nerd Font',
				fontsize= 14,
				foreground= '#ffffff',
				highlight_method= 'block',
				icon_size= 0,
				margin_y= 1,),

######################################################################

########## Cmus Terminal Player ######################################

		widget.Cmus(
				font= 'JetBrains Mono Nerd Font Bold',
				fontsize= 14.5,
				format= '{play_icon}{artist} - {title}',),

######################################################################

########## Kernel Widget #############################################
		
#		widget.GenPollText(
#                 		update_interval = 300,
#                 		func = lambda: subprocess.check_output("printf $(uname -r)",
#				shell=True, text=True),
#                 		foreground = '#ff0000',
#                 		fmt = ' {}',
#				font='JetBrains Mono Nerd Font Bold',
#				fontsize= 14.9,
#				padding= 3,),

######################################################################

########## Separator #################################################

#		widget.Sep(
#				foreground= '#ff9809',
#				background= '#24283b',),

######################################################################

########## Weathher ##################################################

#                widget.OpenWeather(
#            			app_key = "68117e378b7e99a9128ca3625f2d3887",
#            			cityid = "727011",
#            			format = '{icon}  {main_temp:.0f}°C',
#				mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' --hold -e curl wttr.in/Sofia+Bulgaria')},
#            			metric = True,
#            			font = 'JetBrains Mono Nerd Font Bold',
#				fontsize= 14.9,
#				padding= 2,
#            			foreground = '#fcff00',
#				dateformat= '%d-%m-%Y',
#				timeformat= '%H:%M',
#				weather_symbols= owm_symbols,),


######################################################################

########## Weathher V2 ###############################################

		widget.OpenWeather(
				app_key = "68117e378b7e99a9128ca3625f2d3887",
				cityid = "727011",
				format = '{location_city} {icon} {main_temp:.0f}°{units_temperature}',
				mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' --hold -e curl wttr.in/Sofia+Bulgaria')},
				metric = True,
				font = "JetBrainsMono Nerd Font Bold",
				fontsize= 14,
				pading= 3,
				foreground = '#fcff00',
				dateformat= '%d-%m-%Y',
                                weather_symbols= owm_symbols,
				decorations=[
                     			BorderDecoration(
                         			colour = '#ff9809',
                         			border_width = [0, 0, 2, 0],)],
				),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## Computer Thermal Sensors ##################################

		widget.TextBox(
                                text=' ',
                                font='JetBrains Mono Nerd Font Bold',
                                fontsize=16,
                                pading= 0,
                                foreground='#ff0093',
				decorations=[
                     			BorderDecoration(
                         			colour = '#ff0093',
                         			border_width = [0, 0, 2, 0],)],
				),

		widget.ThermalZone(
                                font='JetBrains Mono Nerd Font Bold',
				fontsize= 14,
                                high=75,
                                crit=90,
                                fgcolor_crit='ff0000',
                                fgcolor_high='#ff9809',
                                fgcolor_normal='#bbff00',
                                format=' {temp}°C',
                                format_crit='{temp}°C ',
				decorations=[
                     			BorderDecoration(
                         			colour = '#ff0093',
                         			border_width = [0, 0, 2, 0],)],
				),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## Nvidia Thermar Sensors ####################################

                widget.TextBox(
                                text='󰢮 ',
                                font='JetBrains Mono Nerd Font Bold',
                                fontsize=24,
                                pading= 2,
                                foreground='#00fff5',
				decorations=[
                     			BorderDecoration(
                         			colour = '#00fff5',
                         			border_width = [0, 0, 2, 0],)],
				),

                widget.NvidiaSensors(
                                font='JetBrains Mono Nerd Font Bold',
				fontsize= 14,
				padding= 2,
                                threshold=104,
                                foreground_alert='ff0000',
                                format=' {temp}°C',
				decorations=[
                     			BorderDecoration(
                         			colour = '#00fff5',
                         			border_width = [0, 0, 2, 0],)],
				),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## CPU Usage #################################################

                widget.TextBox(
                                text=' ',
                                font='JetBrains Mono Nerd Font Bold',
                                fontsize= 13,
                                pading= 1,
                                foreground='#fcff00',
				decorations=[
                     			BorderDecoration(
                         			colour = '#fcff00',
                         			border_width = [0, 0, 2, 0],)],
				),

                widget.CPU(
                           	font='JetBrains Mono Nerd Font Bold',
			   	fontsize= 14.5,
			   	foreground = '#bbff00',
                           	format='{load_percent}%',
				decorations=[
                     			BorderDecoration(
                         			colour = '#fcff00',
                         			border_width = [0, 0, 2, 0],)],
				),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),
######################################################################

########## Memmory Usage #############################################

                widget.TextBox(
                                text=' ',
                                font='JetBrains Mono Nerd Font Bold',
                                fontsize=14.5,
                                pading= 0,
                                foreground='#00fff5',
				decorations=[
                     			BorderDecoration(
                         			colour = '#00fff5',
                         			border_width = [0, 0, 2, 0],)],
				),

                widget.Memory(
                              	format='{MemUsed:.0f}{mm}{MemTotal:.0f}{mm}', 
                              	measure_mem='M',
                              	font='JetBrains Mono Nerd Font Bold',
				mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e htop')},
				fontsize= 14.5,
			     	pading= 3,
                              	foreground='#ff9809',
                              	update_interval=0.1,
				decorations=[
                     			BorderDecoration(
                         			colour = '#00fff5',
                         			border_width = [0, 0, 2, 0],)],
				),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## HDD Usage #################################################

		 widget.TextBox(
                                text=' ',
                                font='JetBrains Mono Nerd Font Bold',
                                fontsize=16.5,
                                pading= 3,
                                foreground='#ff0000',
				decorations=[
                     			BorderDecoration(
                         			colour = '#ff0000',
                         			border_width = [0, 0, 2, 0],)],
				),

		widget.DF(
				font= 'JetBrains Mono Nerd Font Bold',
			  	fontsize= 14.5,
			  	foreground= '#bbff00',
			  	format= '{uf}{m}{s}{m}',
			  	measure= 'G',
			  	partition= '/',
			  	update_interval= 60,
			  	visible_on_warn= False,
			  	warn_color= 'ff0000',
			  	warn_space= 2,
				decorations=[
                     			BorderDecoration(
                         			colour = '#ff0000',
                         			border_width = [0, 0, 2, 0],)],
				),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## Volume Controll ###########################################

                widget.TextBox(
                               text=' ',
                               font='JetBrains Mono Nerd Font Bold',
                               fontsize=13.9,
                               pading= 0,
                               foreground='#fcff00',
			       decorations=[
                     			BorderDecoration(
                         			colour = '#fcff00',
                         			border_width = [0, 0, 2, 0],)],
			       ),

                widget.Volume(
                               volume_app='pavucontrol', 
                               mouse_callbacks = {
                               'Button1': lambda: qtile.cmd_spawn("pavucontrol")},
                               font= 'JetBrains Mono Nerd Font Bold',
			       foreground= '#ffffff',
			       fontsize= 14.5,
			       pading= 0,
			       decorations=[
                     			BorderDecoration(
                         			colour = '#fcff00',
                         			border_width = [0, 0, 2, 0],)],
			       ),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 3,
				size_percent= 45),

######################################################################

########## Brightness Controll #######################################

                widget.TextBox(
                               text=' ',
                               font='JetBrains Mono Nerd Font Bold',
                               fontsize=13.3,
                               pading=0,
                               foreground='#ff9809',
			       decorations=[
                     			BorderDecoration(
                         			colour = '#ff9809',
                         			border_width = [0, 0, 2, 0],)],
			       ),

                widget.Backlight(
                               backlight_name='intel_backlight',
                               brightness_file='brightness',
                               max_brightness_file='max_brightness',
                               scroll=True,
                               font= 'JetBrains Mono Nerd Font Bold',
			       fontsize= 14.5,
			       foreground= '#4dff00',
			       decorations=[
                     			BorderDecoration(
                         			colour = '#ff9809',
                         			border_width = [0, 0, 2, 0],)],
			       ),

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## Battery Widget ############################################

                widget.UPowerWidget(
			       battery_height= 12,
			       battery_width= 21,
			       font= 'JetBrains Mono Nerd Font Bold',
			       fontsize= 14.5,
			       foreground= '#00fff5',
			       fill_normal= '#ffffff',
			       fill_low= 'aa00aa',
			       fill_critical= '#ff0000',
			       fill_charge= '#ffffff',
			       border_critical_colour= '#ff0000',
			       border_colour= '#00fff5',
			       border_charge_colour= '#bbff00',
			       margin= 3,
			       text_charging= '{ttf} Charging {percentage:.0f}%',
			       text_discharging= '{tte} Remaining {percentage:.0f}%',
			       decorations=[
                     			BorderDecoration(
                         			colour = '#ffffff',
                         			border_width = [0, 0, 2, 0],)],
			       ),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## Wallpaper Changer #########################################

########## -- Need to install "Feh"

		widget.Wallpaper(
			       directory= '/usr/share/wallpapers/dtos-backgrounds/',
			       font= 'JetBrains Mono Nerd Font Bold',
			       fontsize= 16,
			       foreground= '#ff0093',
			       label= '󰸉',
			       padding= 4,
			       random_selection= True,
			       option= 'fill',
			       wallpaper_command= ['feh', '--bg-fill'],
			       decorations=[
                     			BorderDecoration(
                         			colour = '#0041ff',
                         			border_width = [0, 0, 2.3, 0],)],
			       ),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## System Tray ###############################################

                widget.Systray(
			       padding= 3,
			       font= 'JetBrains Mono Nerd Font Bold',
			       fontsize= 12,
			       icon_size= 15,
			       decorations=[
                     			BorderDecoration(
                         			colour = '#bbff00',
                         			border_width = [0, 0, 2, 0],)],
						padding_x= 1,
			       ),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## Check For Update ##########################################

#		widget.CheckUpdates(
#			       colour_have_updates= '#ff0000',
#			       colour_no_updates= '#bbff00',
#			        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e sudo pacman -Syyu')},
#			       display_format= 'Updates: {updates}',
#			       distro= 'Arch_checkupdates',
#			       font= 'JetBrains Mono Nerd Font Bold',
#		               fontsize= 14,
#			       padding= 4,
#			       foreground= '#fcff00',
#			       initial_text= ' {updates} Avaiable',
#			       no_update_string= '',
#			       update_interval= 1800,),

######################################################################

########## Keyboard Layouts ##########################################

		widget.KeyboardLayout(
                               configured_keyboards=['us','bg phonetic',], 
			       display_map= {'bg phonetic': 'BG',},
                               font='JetBrains Mono Nerd Font Bold',
			       fontsize= 14.5,
			       foreground= '#ff9809',
                               pading= 3,
			       decorations=[
                     			BorderDecoration(
                         			colour = '#ff9809',
                         			border_width = [0, 0, 2, 0],)],
			       ),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## Clock #####################################################

                MouseOverClock(
                               foreground="#00fff5",
                               font='JetBrains Mono Nerd Font Bold',
			       fontsize= 14.5,
			       pading= 2,
			       decorations=[
                     			BorderDecoration(
                         			colour = '#fcff00',
                         			border_width = [0, 0, 2, 0],)],
			       ),

######################################################################

########## Separator #################################################

		widget.Sep(
				foreground= '#24283b',
				background= '#24283b',
				linewidth= 2,
				size_percent= 45),

######################################################################

########## Power Menu ################################################
		
                widget.TextBox(
                    	       text=' 󰐦 ',
                    	       fontsize = 16.5,
                    	       pading= 9,
                    	       font='JetBrains Mono Nerd Font Bold',
                    	       mouse_callbacks= {
                        	   'Button1':
                        	   lambda: qtile.cmd_spawn(os.path.expanduser('~/.config/rofi/powermenu.sh'))
                    	       },
                    	       foreground='#ff0000',
			       decorations=[
                     			BorderDecoration(
                         			colour = '#ff0000',
                         			border_width = [0, 0, 2, 0],)],
                           )
                
            ],
            25,  # height in px
            background="#24283b",  # Bar color
            opacity= 1,            # Bar Transperancy
        ), ),
]

##################################

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
