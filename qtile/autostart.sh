#!/bin/bash

##### Autostarting Programs #####

optimus-manager-qt &
nm-applet --restore &
#nitrogen --restore &
picom -b &
blueman-applet &
xfce4-power-manager &
light-locker &
lxsession &
conky -p10 &
